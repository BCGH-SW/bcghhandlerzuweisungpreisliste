<?php

namespace BcghHandlerZuweisungPreisliste\Subscriber;

use Enlight\Event\SubscriberInterface;

class Frontend implements SubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Register' => [
                ['onFrontendPostDispatch']
            ],
           'Enlight_Controller_Action_PostDispatchSecure_Widgets' => 'Hide_price_index',
            'Shopware_Modules_Admin_Login_FilterResult' => 'filterUserLogin'

        );
    }

    public function onFrontendPostDispatch(\Enlight_Event_EventArgs $args)
    {

        $subject = $args->getSubject();
        $request = $subject->Request();
        $view = $subject->View();

        $view->addTemplateDir(dirname(__DIR__) . '/Resources/views');

        $user_data = Shopware()->Container()->get('modules')->Admin()->sGetUserData();

        if ($request->getActionName() !== 'index') {

            Shopware()->Modules()->Admin()->sLogin(false);
            Shopware()->Db()->query("UPDATE s_user SET `active`=0 , customergroup = 'EK' WHERE id =" . $user_data["additional"]["user"]["id"]);

            $config = Shopware()->Container()->get('shopware.plugin.config_reader')->getByPluginName('BcghHandlerZuweisungPreisliste');
            //check config is not empty
            if(!empty($config)) {
                $adminEmail = isset($config['adminEmail']) ? $config['adminEmail'] : 0;
                //$this->SendAdminNotification($user_data, $adminEmail);

                $customerGroups=Shopware()->Db()->fetchAll("select id,	groupkey, description from  s_core_customergroups");


                $context = [
                    'userEmail' => $user_data["additional"]["user"]["email"],
                    'userCompany' => $user_data["billingaddress"]["company"],
                    'userUstid' => $user_data["billingaddress"]["ustid"],
                    'userFirstname' => $user_data["billingaddress"]["firstname"],
                    'userLastname' => $user_data["billingaddress"]["lastname"],
                    'userStreetnumber' => $user_data["billingaddress"]["street"],
                    'userZzipcode' => $user_data["billingaddress"]["zipcode"],
                    'userCity' => $user_data["billingaddress"]["city"],
                    'userID'   =>$user_data["additional"]["user"]["id"],
                    'customergroups'=>$customerGroups,
                    'shopUrl'=>Shopware()->Shop()->gethost(),
                    'basePath'=>Shopware()->Shop()->getBasePath()
                ];


                 $mail = Shopware()->TemplateMail()->createMail("sBCGHHandlerZuweisungPreisliste",$context);
                 $mail->addTo($adminEmail);
                 $mail->send();

            }
            $errors = ['personal' => "activate account in 2 weeks"];
            $view->assign('errors', $errors);

            $location = ['controller' => 'BcghHandlerZuweisungPreisliste', 'action' => 'thankYouPage'];
            $subject->redirect($location);

            //Shopware()->Container()->get('modules')->Admin()->logout();

        }

    }


    Public function hide_price_index(\Enlight_Event_EventArgs $args) {
        $subject = $args->getSubject();
        $request = $subject->Request();
        $view = $subject->View();
        $view->addTemplateDir(dirname(__DIR__) . '/Resources/views');

        $view->extendsTemplate('frontend/detail/data.tpl');
        $view->extendsTemplate('frontend/detail/index.tpl');
        $view->extendsTemplate('frontend/listing/product-box/box-minimal.tpl');
        $view->extendsTemplate('frontend/listing/product-box/box-basic.tpl');
    }





    public function filterUserLogin(\Enlight_Event_EventArgs $args)
    {
        $email = $args->get('email');
        $originalResult = $args->getReturn();

        $id = Shopware()->Db()->fetchOne("SELECT id FROM s_user WHERE active=0 and customergroup!='EK' and email = ?", [$email]);
        $originalResult[0]="";
        if ($id) {
            $originalResult[0][] = 'Ihr Kundenkonto wurde deaktiviert, bitte wenden Sie sich zwecks Kl?rung pers?nlich an uns!';
            return $originalResult;
        }
    }



}
