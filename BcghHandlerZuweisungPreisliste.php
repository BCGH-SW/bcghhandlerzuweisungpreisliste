<?php

namespace BcghHandlerZuweisungPreisliste;

use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Doctrine\ORM\Tools\SchemaTool;

/**
 * Shopware-Plugin BcghHandlerZuweisungPreisliste.
 */
class BcghHandlerZuweisungPreisliste extends Plugin
{
    /**
     * Adds the widget to the database and creates the database schema.
     *
     * @param Plugin\Context\InstallContext $installContext
     */
    public function install(Plugin\Context\InstallContext $installContext)
    {
        parent::install($installContext);


        $content = file_get_contents( __DIR__  . "/install/email_template_content.txt" );
        $html    = file_get_contents( __DIR__  . "/install/email_template_content_html.txt" );
        $registrationConfirmation_content = file_get_contents( __DIR__  . "/install/registrationConfirmation_email_template_content.txt" );
        $account_accepted_email_content=file_get_contents( __DIR__  . "/install/email_template_sCUSTOMERGROUPHACCEPTED.txt" );
        $account_rejected_email_content=file_get_contents( __DIR__  . "/install/email_template_sCUSTOMERGROUPHREJECTED.txt" );

        // create a new object
        $mail = new \Shopware\Models\Mail\Mail();

        // set all parameters
        $name="sBCGHHandlerZuweisungPreisliste";
        $mail->setName($name);
        $mail->setFromMail( "{config name=mail}" );
        $mail->setFromName( "{config name=shopName}" );
        $mail->setSubject( "New registration notification {config name=shopName}" );
        $mail->setContent($content);
        $mail->setContentHtml($html);
        $mail->setIsHtml(true);
        $mail->setMailtype( 1 );
        $mail->setContext( null );


        // save it
        Shopware()->Models()->persist( $mail );
        Shopware()->Models()->flush();

        Shopware()->Db()->query("update  s_core_config_mails set content='".$registrationConfirmation_content."' where `name`='sREGISTERCONFIRMATION' ");

        Shopware()->Db()->query("update  s_core_config_mails set content='".$account_accepted_email_content."' where `name`='sCUSTOMERGROUPHACCEPTED' ");

        Shopware()->Db()->query("update  s_core_config_mails set content='".$account_rejected_email_content."' where `name`='sCUSTOMERGROUPHREJECTED' ");


        //error message german
        $loginError_message_german="Vielen Dank für Ihre Anmeldung.<br> Wir prüfen aktuell noch Ihre Registrierung und melden uns umgehend bei Ihnen.<br>  Aktuell können Sie sich noch nicht einloggen.";
        Shopware()->Db()->query("update  s_core_snippets set value='".$loginError_message_german."' where localeID=1 and `name`='LoginFailureActive' ");

        //error message english
        $loginError_message_english="Thank you for your registration. <br> We are currently checking your registration and we will get back to you as soon as possible.";
        Shopware()->Db()->query("update  s_core_snippets set value='".$loginError_message_english."' where localeID=2 and `name`='LoginFailureActive' ");



    }

    /**
     * Remove widget and remove database schema.
     *
     * @param Plugin\Context\UninstallContext $uninstallContext
     */
    public function uninstall(Plugin\Context\UninstallContext $uninstallContext)
    {
        parent::uninstall($uninstallContext);

        // delete sBCGHHandlerZuweisungPreisliste email template from table
        Shopware()->Db()->query("delete  from  s_core_config_mails where `name`='sBCGHHandlerZuweisungPreisliste' ");

        // update default sREGISTERCONFIRMATION email template
        $uninstall_registrationConfirmation_content = file_get_contents( __DIR__  . "/uninstall/uninstall_registrationConfirmation_email_template_content.txt" );
        Shopware()->Db()->query("update  s_core_config_mails set content='".$uninstall_registrationConfirmation_content."' where `name`='sREGISTERCONFIRMATION' ");


        // update default sCUSTOMERGROUPHACCEPTED email template
        $uninstall_sCUSTOMERGROUPHACCEPTED_content = file_get_contents( __DIR__  . "/uninstall/uninstall_email_template_sCUSTOMERGROUPHACCEPTED.txt" );
        Shopware()->Db()->query("update  s_core_config_mails set content='".$uninstall_sCUSTOMERGROUPHACCEPTED_content."' where `name`='sCUSTOMERGROUPHACCEPTED' ");


        // update default sCUSTOMERGROUPHREJECTED email template
        $uninstall_sCUSTOMERGROUPHREJECTED_content = file_get_contents( __DIR__  . "/uninstall/uninstall_email_template_sCUSTOMERGROUPHREJECTED.txt" );
        Shopware()->Db()->query("update  s_core_config_mails set content='".$uninstall_sCUSTOMERGROUPHREJECTED_content."' where `name`='sCUSTOMERGROUPHREJECTED' ");



    }

    /**
    * @param ContainerBuilder $container
    */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('bcgh_handler_zuweisung_preisliste.plugin_dir', $this->getPath());
        parent::build($container);
    }

    /**
     * creates database tables on base of doctrine models
     */
    private function createSchema()
    {

    }

    private function removeSchema()
    {

    }




}
