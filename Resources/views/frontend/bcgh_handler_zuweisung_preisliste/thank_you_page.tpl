{extends file='frontend/custom/index.tpl'}
{block name="frontend_index_left_inner"}{/block}
{* Main content *}
{block name="frontend_index_content"}
    <div class="custom-page--content content block">
        {* Custom page tab content *}
        {block name="frontend_custom_article"}
            <div class="content--custom">
                {block name="frontend_custom_article_inner"}
                    {* Custom page tab headline *}
                    {block name="frontend_custom_article_headline"}
                        <h1 class="custom-page--tab-headline">Vielen Dank für Ihre Registrier-Anfrage</h1>
                    {/block}

                    <p>
                     Ihre Registrier-Anfrage bearbeiten wir innerhalb von 2 Werktagen,<br>
                    Sie erhalten automatisch eine Rückmeldung an die angegebene E-Mail Adresse.<br>
                    Bitte beachten Sie das Sie nach der Registrier-Anfrage noch keinen Kauf unserer Produkte tätigen können.
                    </p>
                    <p><a href="{$smarty.server.HTTP_HOST}">Zurück zur Startseite ></a></p>


                {/block}
            </div>
        {/block}
    </div>
{/block}
