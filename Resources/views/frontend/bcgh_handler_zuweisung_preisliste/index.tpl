{extends file='frontend/custom/index.tpl'}
{block name="frontend_index_left_inner"}{/block}
{* Main content *}
{block name="frontend_index_content"}
    <div class="custom-page--content content block">
        {* Custom page tab content *}
        {block name="frontend_custom_article"}
            <div class="content--custom">
                {block name="frontend_custom_article_inner"}
                    {* Custom page tab headline *}
                    {block name="frontend_custom_article_headline"}
                        <h1 class="custom-page--tab-headline">{$message}</h1>
                    {/block}
                {/block}
            </div>
        {/block}
    </div>
{/block}