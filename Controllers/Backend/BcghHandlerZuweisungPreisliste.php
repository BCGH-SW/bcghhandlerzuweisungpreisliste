<?php

/**
 * Backend controllers extending from Shopware_Controllers_Backend_Application do support the new backend components
 */
class Shopware_Controllers_Backend_BcghHandlerZuweisungPreisliste extends Shopware_Controllers_Backend_Application
{
     protected $model = "Shopware\Models\Customer\Customer";
     protected $alias = 'bcgh_handlerpreisliste';

    public function indexAction()
    {
    }
}
