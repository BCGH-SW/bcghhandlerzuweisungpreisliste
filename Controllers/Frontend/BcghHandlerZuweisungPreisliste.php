<?php

/**
 * Frontend controller
 */
class Shopware_Controllers_Frontend_BcghHandlerZuweisungPreisliste extends Enlight_Controller_Action
{


    // check user , group , assign group , activate account and send customer accept email
    public function indexAction()
    {
        if ($this->Request()->getParam("userid") and $this->Request()->getParam("groupid")) {
            $user_id=$this->Request()->getParam("userid");
            $group_id=$this->Request()->getParam("groupid");

            // get group name
            $customerGroupValue=Shopware()->Db()->fetchOne(" select groupkey from s_core_customergroups where id=".$group_id);
            if(!empty($customerGroupValue)){

                // assigning group and active the account
                $groupUpdateStatus=Shopware()->Db()->query(" update s_user set active=1, customergroup='".$customerGroupValue."' where id=".$user_id);
                if($groupUpdateStatus){

                    // send customer accept email
                    $userDetails=Shopware()->Db()->fetchAll(" SELECT s_user.email,s_user.firstname, s_user.lastname,company 
                                                    FROM s_user JOIN s_user_addresses ON s_user_addresses.`user_id`= s_user.`id` WHERE s_user.id=".$user_id);

                    $context = ['firstname' => $userDetails[0]["firstname"],
                        'lastname' => $userDetails[0]["lastname"],
                        'company' => $userDetails[0]["company"]
                    ];

                    $mail = Shopware()->TemplateMail()->createMail("sCUSTOMERGROUPHACCEPTED",$context);
                    $mail->addTo($userDetails[0]["email"]);
                    $mail->send();

                    $groupUpdateMsg="Ihr Händlerkonto wurde erfolgreich freigeschaltet.";

                }else{
                    $groupUpdateMsg="Kein Benutzer gefunden.";
                }
            }else{
                $groupUpdateMsg="Keine Gruppe gefunden.";
            }
        }else{
            $groupUpdateMsg="Keine Gruppe/Benutzer gefunden.";
        }

        // Assign a template variable
        $this->View()->assign('message', $groupUpdateMsg);
    }


    // reject merchant request , delete the record and send customer reject email
    public function rejectuserAction()
    {
        if ($this->Request()->getParam("userid") and $this->Request()->getParam("groupid")) {
            $user_id = $this->Request()->getParam("userid");
            // send sCUSTOMERGROUPHREJECTED email to user
            $userDetails=Shopware()->Db()->fetchAll(" SELECT s_user.email,s_user.firstname, s_user.lastname,company 
                                                    FROM s_user JOIN s_user_addresses ON s_user_addresses.`user_id`= s_user.`id` WHERE s_user.id=".$user_id);

            $context = ['firstname' => $userDetails[0]["firstname"],
                        'lastname' => $userDetails[0]["lastname"],
                        'company' => $userDetails[0]["company"]
            ];


            if($userDetails[0]["email"]){

                $mail = Shopware()->TemplateMail()->createMail("sCUSTOMERGROUPHREJECTED",$context);
                $mail->addTo($userDetails[0]["email"]);
                $mail->send();
            }
            // delete user account
            //Shopware()->Db()->query(" delete from s_user where id=".$user_id);
        }
    }


    public function thankYouPageAction()
    {
        $groupUpdateMsg="";
        $this->View()->assign('message', $groupUpdateMsg);
    }






}
